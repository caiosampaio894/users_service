import express from "express"
import { v1 as uuidv1, v4 as uuidv4, v5 as uuidv5 } from 'uuid';
uuidv1();
uuidv4();
const MY_NAMESPACE = uuidv4();
uuidv5('Hello, World!', MY_NAMESPACE);
const app = express();
app.use(express.json());


const accounts = []

const verifyCpf = (req, res, next) => {
    const {cpf} = req.params
    
    const accountCpf = accounts.some(account => account.cpf == cpf);

    
    if(!accountCpf) {
        return res.status(404).send({ error: "invalid cpf - user is not registered" })
    }

    next()
}

app.post('/users', (req, res) => {
    const {name, cpf} = req.body
    let id = uuidv4()
    const user = {
        id, 
        name,
        cpf,
        notes: []
    }

    accounts.push(user)
    
    res.status(201).send(user)
});


app.get('/users', (req, res) => {
    res.send(accounts)
})


app.patch('/users/:cpf', verifyCpf, (req, res) => {

    const {cpf} = req.params
    
    const accountCpf = accounts.findIndex(account => account.cpf == cpf)

    accounts[accountCpf] = { ...accounts[accountCpf], ...req.body }
    
    res.status(200).send(accounts[accountCpf])
})

app.delete('/users/:cpf', verifyCpf, (req, res) => {

    const {cpf} = req.params
    
    const accountCpf = accounts.findIndex(account => account.cpf == cpf)
    
    if(accounts[accountCpf]) {
        accounts.splice(accountCpf, 1)
    }
    
    let output = {"messagem": "User is deleted",
                   "users": accounts}
    

    res.status(200).send(output)
})



app.post('/users/:cpf/notes', verifyCpf, (req, res) => {
    
    const {cpf} = req.params
    const {title, content} = req.body
    let id = uuidv4()
    let created_at = new Date().toString()

    const accountCpf = accounts.findIndex(account => account.cpf == cpf)

    const notesObj = {
        id,
        created_at,
        title,
        content
    }
    
    accounts[accountCpf].notes.push(notesObj)

    res.status(201).send({ "message": `${title} was added into ${accounts[accountCpf].name}'s notes` })

})


app.get('/users/:cpf/notes', verifyCpf, (req, res) => {
    
    const {cpf} = req.params

    const accountCpf = accounts.findIndex(account => account.cpf == cpf)

    res.status(200).send(accounts[accountCpf].notes)

})

app.patch('/users/:cpf/notes/:id', verifyCpf, (req, res) => {
    
    const {cpf, id} = req.params
    const accountCpf = accounts.findIndex(account => account.cpf == cpf)
    const notesId = accounts[accountCpf].notes.findIndex(notes => notes.id === id)
    let updated_at = new Date().toString()
    accounts[accountCpf].notes = [{ ...accounts[accountCpf].notes[notesId], ...req.body }]


    accounts[accountCpf].notes[notesId]['updated_at'] = updated_at


    res.status(200).send(accounts[accountCpf].notes)
})


app.delete('/users/:cpf/notes/:id', verifyCpf, (req, res) => {
    const {cpf, id} = req.params
    const accountCpf = accounts.findIndex(account => account.cpf == cpf)
    const notesId = accounts[accountCpf].notes.findIndex(notes => notes.id === id)

    if(accounts[accountCpf].notes[notesId]) {
        accounts[accountCpf].notes.splice(notesId, 1)
    }

    res.status(200).send(accounts[accountCpf].notes)

})


app.listen(3000)


